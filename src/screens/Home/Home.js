import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import styles from './Home.Styles.js';

const {
	container,
	homeContainer,
	homeHeading,
	homeButton,
	homeButtonGradient,
	homeImage,
	resqrLogo,
	homeButtonText,
	homeStrip,
} = styles;

export default class App extends Component {
	static navigationOptions = {
		header: null,
	};

	render() {
		const { navigate } = this.props.navigation;
		return (
			<View style={container}>
				<LinearGradient
					colors={['#06beb6', '#1890ff']}
					start={{ x: 0.0, y: 1.0 }}
					end={{ x: 1.0, y: 1.0 }}
					style={homeStrip}
				/>
				<View style={homeContainer}>
					<View style={resqrLogo}>
						<Image
							style={homeImage}
							resizeMode="contain"
							source={require('../../../assets/images/RESQRLogo.png')}
						/>
						<Text style={homeHeading}>Ordering Made Easy</Text>
					</View>
					<LinearGradient
						colors={['#06beb6', '#1890ff']}
						start={{ x: 0.0, y: 1.0 }}
						end={{ x: 0.8, y: 1.0 }}
						style={homeButtonGradient}
					>
						<TouchableOpacity onPress={() => navigate('CategoriesList')} style={homeButton}>
							<Text style={homeButtonText}>Scan QRCode</Text>
						</TouchableOpacity>
					</LinearGradient>
				</View>
			</View>
		);
	}
}
