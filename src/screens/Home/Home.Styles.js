import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  homeContainer: {
    height: '80%',
    width: '100%',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  homeHeading: {
    fontSize: 28,
    color: 'black',
    fontFamily: 'Lato Light',
  },
  homeButtonGradient: {
    width: '80%',
    height: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },
  homeButton: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    backgroundColor: 'transparent',
  },
  resqrLogo: {
    height: '60%',
    alignItems: 'center',
    width: '100%',
  },
  homeImage: {
    width: '70%',
    height: '70%',
    marginBottom: 10,
  },
  homeButtonText: {
    color: 'white',
    fontSize: 18,
    fontFamily: 'Lato',
  },
  homeStrip: {
    width: '100%',
    height: '1%',
  },
});
