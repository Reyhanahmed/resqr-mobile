import React from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

import CategoriesList from './CategoriesList';

const GET_RESTAURANT_QUERY = gql`
	query GetRestaurantQuery($tableId: Int!) {
		getRestaurant(tableId: $tableId) {
			ok
			restaurant {
				id
				name
				restaurantImages {
					id
					url
				}
				categories {
					id
					name
					categoryImage
				}
			}
			errors {
				path
				message
			}
		}
	}
`;

const WrappedCategoriesList = props => (
  <Query query={GET_RESTAURANT_QUERY} variables={{ tableId: 2 }}>
    {getRestaurantQuery => <CategoriesList {...props} getRestaurantQuery={getRestaurantQuery} />}
  </Query>
);

export default WrappedCategoriesList;
