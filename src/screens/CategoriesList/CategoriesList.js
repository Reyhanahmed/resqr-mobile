import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Snackbar from 'react-native-snackbar';
import Spinner from 'react-native-spinkit';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  text: {
    fontSize: 48,
    color: 'black',
    fontWeight: 'bold',
  },
});

const CategoriesList = ({ getRestaurantQuery }) => {
  const { loading, data } = getRestaurantQuery;
  if (loading) {
    return (
      <View style={styles.container}>
        <Spinner size={50} type="Bounce" color="#1890ff" />
      </View>
    );
  }
  const { getRestaurant: { errors, ok, restaurant } } = data;
  if (!ok) {
    Snackbar.show({
      title: `${errors[0].message}`,
      duration: Snackbar.LENGTH_INDEFINITE,
      action: {
        title: 'Dismiss',
        onPress: () => Snackbar.dismiss(),
        color: 'red',
      },
    });
    return null;
  }
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Categories List</Text>
    </View>
  );
};

export default CategoriesList;
