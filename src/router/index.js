import { StackNavigator } from 'react-navigation';

import Home from '../screens/Home';
import CategoriesList from '../screens/CategoriesList';
import QRScanner from '../screens/QRScanner';

export default StackNavigator({
  Home: {
    screen: Home,
  },
  CategoriesList: {
    screen: CategoriesList,
  },
  QRScanner: {
    screen: QRScanner,
  },
});
