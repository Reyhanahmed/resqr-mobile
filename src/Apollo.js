import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
// import { ApolloLink } from 'apollo-link';
// 10.0.2.2
const httpLink = new HttpLink({ uri: 'http:/10.0.2.2:8082/graphql' });

export default new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache(),
});
